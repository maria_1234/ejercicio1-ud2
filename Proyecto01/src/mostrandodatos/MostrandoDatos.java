package mostrandodatos;

import java.util.Scanner;

public class MostrandoDatos {

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		
		System.out.println("Introduce el nombre");
		String nombre=input.nextLine();
		System.out.println("Introduce el apellido");
		String apellido=input.nextLine();
		
		System.out.println("El nombre introducido es "+nombre+" "+apellido);
		
		input.close();

	}

}
